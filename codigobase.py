import pycuda.driver as cuda
import pycuda.autoinit
import numpy
from pycuda.compiler import SourceModule


def minSecuencial(array):
    min = array[0]
    #PROGRAME AQUI
    for e in array:
        if e < min:
            min = e
    return min

def maxSecuencial(array):
    max = array[0]
    #PROGRAME AQUI
    for e in array:
        if e > max:
            max = e
    return max

def sumSecuencial(array):
    sum = 0
    #PROGRAME AQUI
    for e in array:
        sum += e
    return sum

def minParalelo(array):
    a_gpu = cuda.mem_alloc(a.nbytes)
    cuda.memcpy_htod(a_gpu, a)
    mod = SourceModule("""
    __global__ void funcion(int *input){
        //PROGRAME AQUI
        int id = threadIdx.x;
        int salto = 1;
        int hebras = blockDim.x;
        
        while (hebras > 0) {
            if (id < hebras) {
                int index1 = id * salto * 2;
                int index2 = index1 + salto;
                input[index1] = (input[index1] < input[index2]) ? input[index1] : input[index2];
                salto *= 2;
                hebras /= 2;
            }
        }
    }
    """)

    func = mod.get_function("funcion")
    threads = int(len(array)/2)
    func(a_gpu, block=(threads,1,1))
    array_resultado = numpy.empty_like(a)
    cuda.memcpy_dtoh(array_resultado, a_gpu)
    print(array_resultado)
    return array_resultado[0]

def maxParalelo(array):
    a_gpu = cuda.mem_alloc(a.nbytes)
    cuda.memcpy_htod(a_gpu, a)
    mod = SourceModule("""
    __global__ void funcion(int *input){
        //PROGRAME AQUI
        int id = threadIdx.x;
        int salto = 1;
        int hebras = blockDim.x;
        
        while (hebras > 0) {
            if (id < hebras) {
                int index1 = id * salto * 2;
                int index2 = index1 + salto;
                input[index1] = (input[index1] > input[index2]) ? input[index1] : input[index2];
                salto *= 2;
                hebras /= 2;
            }
        }
    }
    """)

    func = mod.get_function("funcion")
    threads = int(len(array)/2)
    func(a_gpu, block=(threads,1,1))
    array_resultado = numpy.empty_like(a)
    cuda.memcpy_dtoh(array_resultado, a_gpu)
    return array_resultado[0]

def sumParalelo(array):
    a_gpu = cuda.mem_alloc(a.nbytes)
    cuda.memcpy_htod(a_gpu, a)
    mod = SourceModule("""
    __global__ void funcion(int *input){
        //PROGRAME AQUI
        int id = threadIdx.x;
        int salto = 1;
        int hebras = blockDim.x;
        
        while (hebras > 0) {
            if (id < hebras) {
                int index1 = id * salto * 2;
                int index2 = index1 + salto;
                input[index1] = input[index1] + input[index2];
                salto *= 2;
                hebras /= 2;
            }
        }
    }
    """)

    func = mod.get_function("funcion")
    threads = int(len(array)/2)
    func(a_gpu, block=(threads,1,1))
    array_resultado = numpy.empty_like(a)
    cuda.memcpy_dtoh(array_resultado, a_gpu)
    return array_resultado[0]

a = numpy.random.randint(10000000, size=8)
a = a.astype(numpy.int32)

print(a)
print(minSecuencial(a))
print(maxSecuencial(a))
print(sumSecuencial(a))
print()
print(minParalelo(a))
print(maxParalelo(a))
print(sumParalelo(a))