# Test 1 Laboratorio

## Objetivo

El objetivos de este laboratorio es:

* Implementar algoritmos secuenciales y paralelos en GPU utilizando el patrón paralelo Reduce.

## Ejercicios

1. Descargar y ejecutar el código proporcionado como base para el ejercicio.
2. Implementar tres funciones secuenciales que retornen el elemento más grande, el más
pequeño y la suma de todos los elementos.
3. Implementar las tres funciones anteriores en paralelo (GPU) utilizando el patrón Re-
duce.
